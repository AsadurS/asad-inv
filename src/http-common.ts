import axios from "axios";
const apiClient = axios.create({
  baseURL: "https://api.bookandread.com/",
  headers: {
    "Content-type": "application/json",
     'token'      : 'asad'
  },
});
export default apiClient;