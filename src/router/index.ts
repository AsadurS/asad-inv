import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import asadRoute from './asad'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView,
      meta:{
        title:'asad'
      }
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/AboutView.vue'),
      meta:{
        title:'about'
      }
    },
    ...asadRoute
  ]
})
router.beforeEach((to,from, next)=>{
  console.log(to.meta.title,from, next)
  next()
})
export default router
