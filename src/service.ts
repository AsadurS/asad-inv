import http from './http-common'
class HttpService{
    get(url:string):Promise<any>{
    return http.get(url);
    }
    delete(url:string, data:Object):Promise<any>{
        return http.delete(url, data);
    }
    post(url:string, data:object):Promise<any>{
        return http.post(url, data);
    }
    put(url:string, data:object):Promise<any>{
        return http.put(url, data);
    }
    patch(url:string, data:object):Promise<any>{
        return http.patch(url, data);
    }
}

export default new HttpService();
